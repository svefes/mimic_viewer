This repository contains the current version of the MIMIC_viewer tool implemented with KNIME.
In order to work with this workflow you need to install KNIME and a MySQL version of the MIMIC III DB locally.
Before the first usage adapt the Database Connector enclosed into the DB Connector node.
The Database URL has to point to the local copy of MIMIC III.
